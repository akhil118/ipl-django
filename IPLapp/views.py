from django.shortcuts import render
from IPLapp.models import Matches,Deliveries
from django.db.models import Count,Sum


# Create your views here.
def home(request):
    return render(request,'home.html')

def playedperyear(request):
    result=Matches.objects.values('team1').annotate(win=Count('team1')).order_by('-win')
    return render(request,'playedperyear.html',{'result':result})

def wonperyear(request):
    result=Matches.objects.values('season','winner').annotate(win=Count('winner')).order_by('season','-win')
    return render(request,'wonperyear.html',{'result':result})



def extrarunsin2016(request):
    result=Deliveries.objects.filter(match_id__season=2016).values('batting_team').annotate(extraruns=Sum('extra_runs')).order_by('extraruns')
    return render(request,'extrarunsin2016.html',{'result':result})



def economicbowler(request):

    result = Deliveries.objects.filter(match_id__season=2015).values('bowler').annotate(economy=Sum('total_runs')*6/Count('total_runs')).order_by('economy')[:10]
    return render(request,'top10economybowlingin2015.html',{'result':result})
