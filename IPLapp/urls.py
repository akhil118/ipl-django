from django.urls import path
from . import views

urlpatterns = [
    path('', views.home,name='home'),
    path('played', views.playedperyear,name="matchesplayed"),
    path('won', views.wonperyear,name='wonperyear'),
    path('runs', views.extrarunsin2016 ,name='extraruns'),
    path('bowler', views.economicbowler,name='economicbowler'),

]
